use std::ops::{ Add, Sub };

static SEC_TO_MS: f64 = 1000.0;
static MIN_TO_SEC: f64 = 60.0;
static HOUR_TO_MIN: f64 = 60.0;
static DAY_TO_HOUR: f64 = 24.0;
static WEEK_TO_DAY: f64 = 7.0;

// Implements add and subtract for unnamed structure fields
macro_rules! impl_add_sub {
    ($t:ident) => {
        impl Add<$t> for $t {
            type Output = $t;
            fn add(self, rhs: $t) -> Self::Output {
                $t(self.0+rhs.0)
            }
        }

        impl Add<&$t> for &$t {
            type Output = $t;
            fn add(self, rhs: &$t) -> Self::Output {
                $t(self.0+rhs.0)
            }
        }

        impl Sub<$t> for $t {
            type Output = $t;
            fn sub(self, rhs: $t) -> Self::Output {
                $t(self.0-rhs.0)
            }
        }

        impl Sub<&$t> for &$t {
            type Output = $t;
            fn sub(self, rhs: &$t) -> Self::Output {
                $t(self.0-rhs.0)
            }
        }
    };
}

#[derive(PartialEq, Debug, Clone)]
pub struct MilliSec(pub f64);
impl_add_sub!(MilliSec);
impl From<Seconds> for MilliSec {
    fn from(item: Seconds) -> Self {
        MilliSec(item.0*SEC_TO_MS)
    }
}
impl TimeCast for MilliSec {
    type Down = MilliSec;
    type Up = Seconds;
    fn down(&self) -> (Self, MilliSec) {
        let floor = self.0.floor();
        (MilliSec(floor), MilliSec(self.0-floor))
    }
    fn up(&self) -> (Seconds, Self) {
        let seconds = Seconds::from(self.clone());
        seconds.down()
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Seconds(pub f64);
impl_add_sub!(Seconds);
impl From<MilliSec> for Seconds {
    fn from(item: MilliSec) -> Self {
        Seconds(item.0/SEC_TO_MS)
    }
}
impl From<Minutes> for Seconds {
    fn from(item: Minutes) -> Self {
        Seconds(item.0*MIN_TO_SEC)
    }
}
impl From<Hours> for Seconds {
    fn from(item: Hours) -> Self {
        Minutes::from(item).into()
    }
}
impl From<Days> for Seconds {
    fn from(item: Days) -> Self {
        Minutes::from(Hours::from(item)).into()
    }
}
impl From<Weeks> for Seconds {
    fn from(item: Weeks) -> Self {
        Minutes::from(Hours::from(Days::from(item))).into()
    }
}
impl TimeCast for Seconds {
    type Down = MilliSec;
    type Up = Minutes;
    fn down(&self) -> (Self, MilliSec) {
        let floor = self.0.floor();
        (Seconds(floor), Seconds(self.0-floor).into())
    }
    fn up(&self) -> (Minutes, Self) {
        let minutes = Minutes::from(self.clone());
        minutes.down()
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Minutes(pub f64);
impl_add_sub!(Minutes);
impl From<Seconds> for Minutes {
    fn from(item: Seconds) -> Minutes {
        Minutes(item.0/MIN_TO_SEC)
    }
}
impl From<Hours> for Minutes {
    fn from(item: Hours) -> Self {
        Minutes(item.0*HOUR_TO_MIN)
    }
}
impl TimeCast for Minutes {
    type Down = Seconds;
    type Up = Hours;
    fn down(&self) -> (Self, Seconds) {
        let floor = self.0.floor();
        (Minutes(floor), Minutes(self.0-floor).into())
    }
    fn up(&self) -> (Hours, Self) {
        let hours = Hours::from(self.clone());
        hours.down()
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Hours(pub f64);
impl_add_sub!(Hours);
impl From<Minutes> for Hours {
    fn from(item: Minutes) -> Self {
        Hours(item.0/HOUR_TO_MIN)
    }
}
impl From<Days> for Hours {
    fn from(item: Days) -> Hours {
        Hours(item.0*DAY_TO_HOUR)
    }
}
impl TimeCast for Hours {
    type Down = Minutes;
    type Up = Days;
    fn down(&self) -> (Self, Minutes) {
        let floor = self.0.floor();
        (Hours(floor), Hours(self.0-floor).into())
    }
    fn up(&self) -> (Days, Self) {
        let days = Days::from(self.clone());
        days.down()
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Days(pub f64);
impl_add_sub!(Days);
impl From<Hours> for Days {
    fn from(item: Hours) -> Days {
        Days(item.0/DAY_TO_HOUR)
    }
}
impl From<Weeks> for Days {
    fn from(item: Weeks) -> Days {
        Days(item.0*WEEK_TO_DAY)
    }
}
impl TimeCast for Days {
    type Down = Hours;
    type Up = Weeks;
    fn down(&self) -> (Self, Hours) {
        let floor = self.0.floor();
        (Days(floor), Days(self.0-floor).into())
    }
    fn up(&self) -> (Weeks, Self) {
        let weeks = Weeks::from(self.clone());
        weeks.down()
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Weeks(pub f64);
impl_add_sub!(Weeks);
impl From<Days> for Weeks {
    fn from(item: Days) -> Weeks {
        Weeks(item.0/WEEK_TO_DAY)
    }
}
impl TimeCast for Weeks {
    type Down = Days;
    type Up = Weeks;
    fn down(&self) -> (Self, Days) {
        let floor = self.0.floor();
        (Weeks(floor), Weeks(self.0-floor).into())
    }
    fn up(&self) -> (Weeks, Self) {
        let floor = self.0.floor();
        (Weeks(floor), Weeks(self.0-floor))
    }
}

pub trait TimeCast: Sized {
    type Down;
    type Up;
    fn down(&self) -> (Self, Self::Down);
    fn up(&self) -> (Self::Up, Self);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sec_to_ms() {
        let sec = Seconds::from(MilliSec(1000.0));
        assert_eq!(sec.0, 1.0);
    }

    #[test]
    fn min_to_sec() {
        let min = Minutes::from(Seconds(60.0));
        assert_eq!(min.0, 1.0)
    }

    #[test]
    fn hour_to_min() {
        let hour = Hours::from(Minutes(60.0));
        assert_eq!(hour.0, 1.0);
    }

    #[test]
    fn day_to_hour() {
        let day = Days::from(Hours(24.0));
        assert_eq!(day.0, 1.0)
    }

    #[test]
    fn week_to_day() {
        let week = Weeks::from(Days(7.0));
        assert_eq!(week.0, 1.0)
    }

    #[test]
    fn week_to_sec() {
        let seconds = Seconds::from(Weeks(1.0));
        assert_eq!(seconds.0, 7.0*24.0*60.0*60.0)
    }

    #[test]
    fn sec_to_week() {
        let week = Weeks::from(Days::from(Hours::from(Minutes::from(Seconds(7.0*24.0*60.0*60.0)))));
        assert_eq!(week, Weeks(1.0))
    }

    #[test]
    fn one_week_three_day() {
        let weeks = Weeks::from(Days(10.0));
        let (week, day) = weeks.down();
        assert_eq!(week, Weeks(1.0));
        assert_eq!(day, Days(3.0))
    }

    #[test]
    fn sec_add_results_min() {
        let half_1 = Seconds(30.0);
        let half_2 = Seconds(30.0);
        let sec_sum = half_1 + half_2;
        assert_eq!(Minutes::from(sec_sum), Minutes(1.0));
    }

    #[test]
    fn day_sub_results_week() {
        let eight = Days(8.0);
        let one = Days(1.0);
        let day_diff = eight - one;
        assert_eq!(Weeks::from(day_diff), Weeks(1.0))
    }

    #[test]
    fn subtract_with_borrowed() {
        let s1 = Seconds(1.0);
        let s2 = Seconds(1.0);
        let res = &s1 - &s2;
        assert_eq!(res, Seconds(0.0));
    }
}