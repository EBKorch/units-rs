use super::pixel::PixelPoint;

pub trait Collide<T> {
    fn collide(&self, other: T) -> bool;
}

pub trait Distance<T> {
    fn distance(&self, other: T) -> f64;
}

pub trait InPixels<T> {
    fn in_pixels(self, scale: &f64, offset: &PixelPoint) -> T;
}

pub trait InMeters<T> {
    fn in_meters(self, scale: &f64, offset: &PixelPoint) -> T;
}
