use std::ops::Sub;

use super::{
    traits::InPixels,
    pixel::{ Pixels, PixelPoint },
};

#[derive(Clone)]
pub struct Meters(pub f64);
impl Into<Centimeters> for Meters { fn into(self) -> Centimeters { Centimeters(self.0*100.0) }}
impl Into<Millimeters> for Meters { fn into(self) -> Millimeters { Millimeters(self.0*1000.0) }}
impl InPixels<Pixels> for Meters {
    fn in_pixels(self, scale: &f64, _: &PixelPoint) -> Pixels {
        Pixels(self.0*scale)
    }
}
impl Sub<Meters> for Meters {
    type Output = Meters;
    fn sub(self, rhs: Meters) -> Meters {
        Meters(self.0 - rhs.0)
    }
}

pub struct Centimeters(pub f64);
impl Into<Meters> for Centimeters { fn into(self) -> Meters { Meters(self.0/100.0) } }
impl Into<Millimeters> for Centimeters { fn into(self) -> Millimeters { Millimeters(self.0*10.0) }}

pub struct Millimeters(pub f64);
impl Into<Meters> for Millimeters { fn into(self) -> Meters { Meters(self.0/1000.0) }}
impl Into<Centimeters> for Millimeters { fn into(self) -> Centimeters { Centimeters(self.0/10.0) }}

#[derive(Clone)]
pub struct Point {
    pub x: Meters,
    pub y: Meters,
}

impl Point {
    pub fn new<T: Into<Meters>, V: Into<Meters>>(x: T, y: V) -> Point {
        Point { x: x.into(), y: y.into() }
    }
}

impl Sub<Point> for Point {
    type Output = (Meters, Meters);
    fn sub(self, rhs: Point) -> (Meters, Meters) {
        (self.x-rhs.x, self.y-rhs.y)
    }
}

impl InPixels<PixelPoint> for Point {
    fn in_pixels(self, scale: &f64, offset: &PixelPoint) -> PixelPoint {
        PixelPoint { x: self.x.in_pixels(scale, offset)+offset.x.clone(), y: self.y.in_pixels(scale, offset)+offset.y.clone()}
    }
}
