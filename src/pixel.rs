use std::ops::{ Add, Sub };
use super::{
    traits::InMeters,
    metric::{ Meters, Point },
};

#[derive(Clone)]
pub struct Pixels(pub f64);

impl Add<Pixels> for Pixels {
    type Output = Pixels;
    fn add(self, rhs: Pixels) -> Pixels { Pixels(self.0 + rhs.0) }
}
impl Sub<Pixels> for Pixels {
    type Output = Pixels;
    fn sub(self, rhs: Pixels) -> Pixels { Pixels(self.0 - rhs.0) }
}

impl InMeters<Meters> for Pixels {
    fn in_meters(self, scale: &f64, _: &PixelPoint) -> Meters {
        Meters(self.0/scale)
    }
}

#[derive(Clone)]
pub struct PixelPoint {
    pub x: Pixels,
    pub y: Pixels,
}

impl Sub<PixelPoint> for PixelPoint {
    type Output = PixelPoint;
    fn sub(self, rhs: PixelPoint) -> PixelPoint {
        PixelPoint {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Add<PixelPoint> for PixelPoint {
    type Output = PixelPoint;
    fn add(self, rhs: PixelPoint) -> PixelPoint {
        PixelPoint {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl InMeters<Point> for PixelPoint {
    fn in_meters(self, scale: &f64, offset: &PixelPoint) -> Point {
        Point {
            x: Meters((self.x - offset.x.clone()).in_meters(scale, offset).0),
            y: Meters((self.y - offset.y.clone()).in_meters(scale, offset).0),
        }
    }
}
