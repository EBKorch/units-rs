use super::traits::Collide;
use super::traits::Distance;
use super::shape::Shape;

#[derive(Copy, Clone)]
pub struct Point2D {
    pub x: f64,
    pub y: f64,
}

impl Point2D {
    pub fn new(x: f64, y: f64) -> Self {
        Point2D { x, y }
    }
}

impl Collide<&Point2D> for Point2D {
    fn collide(&self, other: &Point2D) -> bool {
        if self.x == other.x && self.y == other.y { true } else { false }
    }
}

impl Collide<&Shape> for Point2D {
    fn collide(&self, other: &Shape) -> bool {
        match other {
            Shape::Rectangle(rectangle) => rectangle.collide(self),
            Shape::Point(point) => point.collide(self),
        }
    }
}

impl Distance<&Point2D> for Point2D {
    fn distance(&self, other: &Point2D) -> f64 {
        ((self.x-other.x).powi(2) + (self.y-other.y).powi(2)).sqrt()
    }
}