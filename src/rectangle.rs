use super::point::Point2D;
use super::shape::Shape;
use super::traits::Collide;

pub struct Rectangle {
    bottom_left: Point2D,
    top_right: Point2D,
    bottom_right: Point2D,
    top_left: Point2D,
}

impl Rectangle {
    pub fn new(x1: f64, y1: f64, x2: f64, y2: f64) -> Self {
        let (x_start, x_end) = if x1 < x2 { (x1, x2) } else { (x2, x1) };
        let (y_start, y_end) = if y1 < y2 { (y1, y2) } else { (y2, y1) };
        Rectangle::from_ordered_point(
            Point2D::new(x_start, y_start),
            Point2D::new(x_end, y_end)
        )
    }

    fn from_ordered_point(bottom_left: Point2D, top_right: Point2D) -> Self {
        let left = bottom_left.x;
        let right = top_right.x;
        let top = top_right.y;
        let bottom = bottom_left.y;
        Rectangle {
            bottom_left,
            top_right,
            bottom_right: Point2D::new(right, bottom),
            top_left: Point2D::new(left, top),
        }
    }

    pub fn from_point(start: Point2D, end: Point2D) -> Self {
        let x1 = start.x; let y1 = start.y;
        let x2 = end.x; let y2 = end.y;
        Rectangle::new(x1, y1, x2, y2)
    }

    pub fn start(&self) -> &Point2D {
        &self.bottom_left
    }

    pub fn end(&self) -> &Point2D {
        &self.top_right
    }

    pub fn points(&self) -> [&Point2D; 4] {
        [
            &self.bottom_left,
            &self.bottom_right,
            &self.top_right,
            &self.top_left,
        ]
    }

    pub fn width(&self) -> f64 {
        self.bottom_right.x - self.bottom_left.x
    }

    pub fn height(&self) -> f64 {
        self.top_left.y - self.bottom_left.y
    }
}

impl Collide<&Point2D> for Rectangle {
    fn collide(&self, other: &Point2D) -> bool {
        if other.x >= self.bottom_left.x && other.y >= self.bottom_left.y &&
           other.x <= self.top_right.x && other.y <= self.top_right.y {
            true
        } else {
            false
        }
    }
}

impl Collide<&Rectangle> for Rectangle {
    fn collide(&self, other: &Rectangle) -> bool {
        for point in other.points().iter() {
            if self.collide(*point) { return true }
        }
        for point in self.points().iter() {
            if other.collide(*point) { return true }
        }
        false
    }
}

impl Collide<&Shape> for Rectangle {
    fn collide(&self, other: &Shape) -> bool {
        match other {
            Shape::Rectangle(rectangle) => self.collide(rectangle),
            Shape::Point(point) => self.collide(point),
        }
    }
}
