pub mod point;
pub mod shape;
pub mod rectangle;
pub mod traits;
pub mod metric;
pub mod pixel;
pub mod time;

#[cfg(test)]
mod tests {
    use super::{
        point::Point2D,
        rectangle::Rectangle,
        shape::Shape,
        traits::Collide,
    };

    #[test]
    fn point_collides_point() {
        let p1 = Point2D::new(0.0, 0.0);
        let p2 = Point2D::new(0.0, 0.0);
        assert_eq!(p1.collide(&p2), true);
    }

    #[test]
    fn point_not_collides_point() {
        let p1 = Point2D::new(0.0, 0.0);
        for (px, py) in vec![(0.0, 1.0), (1.0, 0.0), (1.0, 1.0), (-1.0, 1.0)] {
            assert_eq!(p1.collide(&Point2D::new(px, py)), false);
        }
    }

    #[test]
    fn point_collides_rectangle() {
        let x = 0.5;
        let y = 0.5;
        let rect = Rectangle::new(-x, -y, x, y);
        for (px, py) in vec![(-x, -y), (x, y), (x, -y), (-x, y), (0.0, 0.0)] {
            assert_eq!(rect.collide(&Point2D::new(px, py)), true);
        }
    }

    #[test]
    fn point_not_collides_rectangle() {
        let rect = Rectangle::new(-0.5, -0.5, 0.5, 0.5);
        for (px, py) in vec![(-0.9, -0.9), (-0.9, 0.9), (0.9, -0.9), (0.25, -0.9), (0.9, 0.25)] {
            assert_eq!(rect.collide(&Point2D::new(px, py)), false);
        }
    }

    #[test]
    fn rectangle_collides_rectangle() {
        let rect1 = Rectangle::new(0.0, 0.0, 1.0, 1.0);
        let rect2 = Rectangle::new(0.5, 0.5, 2.0, 2.0);
        let rect3 = Rectangle::new(0.1, -0.1, 2.0, 0.1);
        let rect4 = Rectangle::new(-2.0, -2.0, 2.0, 2.0);
        assert_eq!(rect1.collide(&rect2), true);
        assert_eq!(rect1.collide(&rect3), true);
        assert_eq!(rect1.collide(&rect4), true);
    }

    #[test]
    fn rectangle_invariant() {
        let (lower_x, lower_y) =  (-1.0, -2.0);
        let (upper_x, upper_y) = (2.0, 1.0);
        let r1 = Rectangle::new(lower_x, lower_y, upper_x, upper_y);
        let r2 = Rectangle::new(upper_x, upper_y, lower_x, lower_y);
        assert_eq!(r1.start().x, r2.start().x);
        assert_eq!(r1.start().y, r2.start().y);
        assert_eq!(r1.end().x, r2.end().x);
        assert_eq!(r1.end().y, r2.end().y);
    }

    #[test]
    fn shape_collide_shape() {
        let shape1 = Shape::Point(Point2D::new(0.0, 0.0));
        let shape2 = Shape::Rectangle(Rectangle::new(-1.0, -1.0, 1.0, 1.0));
        assert_eq!(shape1.collide(&shape2), true);
        assert_eq!(shape2.collide(&shape1), true);
    }
}