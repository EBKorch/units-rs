use super::point::Point2D;
use super::rectangle::Rectangle;
use super::traits::Collide;

pub enum Shape {
    Point(Point2D),
    Rectangle(Rectangle),
}

impl Collide<&Shape> for Shape {
    fn collide(&self, other: &Shape) -> bool {
        match self {
            Shape::Rectangle(rectangle) => rectangle.collide(other),
            Shape::Point(point) => point.collide(other),
        }
    }
}
