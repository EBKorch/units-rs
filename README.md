# Units

This project implements pixel and metric units such as distances and points.
Conversions between pixel and metric units is possible, helping drawing objects
in metric units on a pixel canvas.
